﻿using System;


namespace arrays1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Введите n:");
                // Количество элементов (вводится с клавиатуры)
                int n = Convert.ToInt32(Console.ReadLine());

                // Инициализируем массив с длиной n
                double[] array = new double[n];
                // Инициализируем класс для генерации случайных чисел
                var rand = new Random();
                // Заполняем массив случайными вещественными числами
                // Параллельно выводим его элементы и вычисляем сумму
                double sum = 0;
                Console.WriteLine("Исходный массив: ");
                for (int i = 0; i < n; i++)
                {
                    // Округляем вещественное число до 2-х цифр после запятой (используя функцию Math.Round)
                    array[i] = Math.Round(rand.NextDouble(), 2);
                    Console.Write(array[i] + " ");
                    sum += array[i];
                }
                Console.WriteLine();
                Console.WriteLine("Сумма элементов массива: " + sum);
            }
        }
    }

}
